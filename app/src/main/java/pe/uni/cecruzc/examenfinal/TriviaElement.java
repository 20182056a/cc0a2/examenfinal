package pe.uni.cecruzc.examenfinal;

public class TriviaElement {
    String question;
    boolean answer;
    Integer image;

    public TriviaElement(String question, boolean answer, Integer image) {
        this.question = question;
        this.answer = answer;
        this.image = image;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
