package pe.uni.cecruzc.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    File logFile;

    TriviaGame triviaGame;

    TextView questionTextView;
    ImageView questionImageView;
    RadioButton trueRadioButton, falseRadioButton;
    Button backButton, nextButton;

    int position;
    boolean answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logFile = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "file_logger_demo");

        appendLog("onCreate");

        triviaGame = new TriviaGame(MainActivity.this);
        triviaGame.loadData();

        questionTextView = findViewById(R.id.question_text_view);
        questionImageView = findViewById(R.id.question_image_view);
        trueRadioButton = findViewById(R.id.true_radio_button);
        falseRadioButton = findViewById(R.id.false_radio_button);
        backButton = findViewById(R.id.back_button);
        nextButton = findViewById(R.id.next_button);

        changePositionTo(position);

        backButton.setOnClickListener(v -> changePositionTo(--position));

        nextButton.setOnClickListener(v -> changePositionTo(++position));

        trueRadioButton.setOnClickListener(v -> checkAnswer(true));

        falseRadioButton.setOnClickListener(v -> checkAnswer(false));

    }

    private void changePositionTo(int position) {

        appendLog("changePositionTo: " + position);

        if (position == 0) {
            backButton.setClickable(false);
        }
        else if (position == triviaGame.MAX_QUESTIONS - 1) {
            nextButton.setClickable(false);
        }
        else {
            backButton.setClickable(true);
            nextButton.setClickable(true);
        }

        questionTextView.setText(triviaGame.getQuestionFromElement(position));
        answer = triviaGame.getAnswerFromElement(position);
        questionImageView.setImageResource(triviaGame.getImageFromElement(position));
        trueRadioButton.setChecked(false);
        trueRadioButton.setChecked(false);

    }

    private void checkAnswer(boolean userAnswer) {

        appendLog("checkAnswer (UserAnswer: " + userAnswer + " CorrectAnswer: " + answer);

        int stringResId;
        if(userAnswer == answer) {
            stringResId = R.string.correct_msg_snack_bar;
        }
        else {
            stringResId = R.string.incorrect_correct_msg_snack_bar;
        }

        Snackbar
                .make(questionTextView, stringResId, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_snack_bar_text, v1 -> {})
                .show();
    }

    public void appendLog(String text) {

        if (!logFile.exists())
        {
            try {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        try
        {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        appendLog("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        appendLog("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        appendLog("onDestroy");
    }
}