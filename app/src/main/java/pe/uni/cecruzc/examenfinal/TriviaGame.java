package pe.uni.cecruzc.examenfinal;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

public class TriviaGame {
    Context context;
    final int MAX_QUESTIONS = 6;
    ArrayList<TriviaElement> triviaQuestions = new ArrayList<>();

    public TriviaGame(Context context) {
        this.context = context;
    }

    public void loadData() {
        String[] questions = context.getResources().getStringArray(R.array.questions);

        triviaQuestions.add(new TriviaElement(questions[0], false, R.drawable.torre_eiffel));
        triviaQuestions.add(new TriviaElement(questions[1], true, R.drawable.relampagos));
        triviaQuestions.add(new TriviaElement(questions[2], true, R.drawable.vaticano));
        triviaQuestions.add(new TriviaElement(questions[3], false, R.drawable.melbourne));
        triviaQuestions.add(new TriviaElement(questions[4], false, R.drawable.penicilina));
        triviaQuestions.add(new TriviaElement(questions[5], true, R.drawable.monte_fuji));

    }

    public String getQuestionFromElement(int position) {
        return triviaQuestions.get(position).question;
    }

    public boolean getAnswerFromElement(int position) {
        return triviaQuestions.get(position).answer;
    }

    public Integer getImageFromElement(int position) {
        return triviaQuestions.get(position).image;
    }
}
